Java Utility For Typed Retrieval of Properties

Copyright (c) Vladimir Dzhuvinov, 2010 - 2024

README

This package provides a Java utility for typed retrieval of
java.util.Properties as boolean, integer, floating point, string, string list,
enum and URL values.


Package requirements:

	* Java 8+


[EOF]
