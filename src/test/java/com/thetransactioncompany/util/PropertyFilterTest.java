package com.thetransactioncompany.util;


import java.util.Map;
import java.util.Properties;

import junit.framework.TestCase;


/**
 * Tests the property filter.
 */
public class PropertyFilterTest extends TestCase {
	

	public void testFilter_null() {

		assertTrue(PropertyFilter.filterWithPrefix("prefix.", null).isEmpty());
	}


	public void testFilter_empty() {

		assertTrue(PropertyFilter.filterWithPrefix("prefix.", new Properties()).isEmpty());
	}


	public void testFilter_multiple() {

		Properties props = new Properties();
		props.put("prefix.a", "apples");
		props.put("prefix.b", "broccoli");
		props.put("c", "cider");

		Properties out = PropertyFilter.filterWithPrefix("prefix.", props);

		assertEquals("apples", out.getProperty("prefix.a"));
		assertEquals("broccoli", out.getProperty("prefix.b"));
		assertEquals(2, out.size());
	}
	
	
	public void testFilterMap_null() {
		
		assertTrue(PropertyFilter.filterWithPrefixIntoMap("prefix.", null).isEmpty());
	}
	
	
	public void testFilterMap_empty() {
		
		assertTrue(PropertyFilter.filterWithPrefixIntoMap("prefix.", new Properties()).isEmpty());
	}
	
	
	public void testFilterMap_multiple() {
		
		Properties props = new Properties();
		props.setProperty("config.client.1.url", "https://example.com/token");
		props.setProperty("config.client.1.clientID", "123");
		props.setProperty("config.client.1.secret", "quahPho9juto");
		props.setProperty("config.client.2.url", "https://example.org/token");
		props.setProperty("config.client.2.clientID", "456");
		props.setProperty("config.client.2.secret", "AhshiSabae2k");
		
		Map<String, Properties> map = PropertyFilter.filterWithPrefixIntoMap("config.client.", props);
		
		Properties one = new Properties();
		one.setProperty("url", "https://example.com/token");
		one.setProperty("clientID", "123");
		one.setProperty("secret", "quahPho9juto");
		
		Properties two = new Properties();
		two.setProperty("url", "https://example.org/token");
		two.setProperty("clientID", "456");
		two.setProperty("secret", "AhshiSabae2k");
		
		assertEquals(one, map.get("1"));
		assertEquals(two, map.get("2"));
		
		assertEquals(2, map.size());
	}
	
	
	public void testFilterMap_multiple_withMixOfOthers() {
		
		Properties props = new Properties();
		props.setProperty("config.client.enable", "true"); // ignore
		props.setProperty("config.client.1", "client-1"); // ignore
		props.setProperty("config.client.1.", "client-1-dot"); // ignore
		props.setProperty("config.client.1.url", "https://example.com/token");
		props.setProperty("config.client.1.clientID", "123");
		props.setProperty("config.client.1.secret", "quahPho9juto");
		props.setProperty("config.client.2.url", "https://example.org/token");
		props.setProperty("config.client.2.clientID", "456");
		props.setProperty("config.client.2.secret", "AhshiSabae2k");
		props.setProperty("config.client.2.timeout", ""); // ok
		
		Map<String, Properties> map = PropertyFilter.filterWithPrefixIntoMap("config.client.", props);
		
		Properties one = new Properties();
		one.setProperty("url", "https://example.com/token");
		one.setProperty("clientID", "123");
		one.setProperty("secret", "quahPho9juto");
		
		Properties two = new Properties();
		two.setProperty("url", "https://example.org/token");
		two.setProperty("clientID", "456");
		two.setProperty("secret", "AhshiSabae2k");
		two.setProperty("timeout", "");
		
		assertEquals(one, map.get("1"));
		assertEquals(two, map.get("2"));
		
		assertEquals(2, map.size());
	}
}
