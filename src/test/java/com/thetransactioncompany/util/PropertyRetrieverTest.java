package com.thetransactioncompany.util;


import java.net.URI;
import java.net.URL;
import java.util.*;

import junit.framework.TestCase;


public class PropertyRetrieverTest extends TestCase {


	public void testGetBoolean()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.enable", "true");
		props.setProperty("app.reload", "false");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertTrue(pr.getBoolean("app.enable"));
		assertFalse(pr.getBoolean("app.reload"));

		try {
			pr.getBoolean("app.disable");
			fail();
		} catch (PropertyParseException e) {
			assertEquals("app.disable", e.getPropertyKey());
		}
	}


	public void testGetBoolean_upperCase()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.enable", "TRUE");
		props.setProperty("app.reload", "FALSE");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertTrue(pr.getBoolean("app.enable"));
		assertFalse(pr.getBoolean("app.reload"));
	}


	public void testGetOptBoolean()
		throws Exception {

		PropertyRetriever pr = new PropertyRetriever(new Properties());
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertTrue(pr.getOptBoolean("app.enable", true));
	}


	public void testGetOptBooleanEmpty()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.enable", " ");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertTrue(pr.getOptBoolean("app.enable", true));
	}


	public void testGetOptionalBoolean()
		throws Exception {

		PropertyRetriever pr = new PropertyRetriever(new Properties());
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertTrue(pr.getOptBoolean("app.enable", Optional.of(true)).get());
		assertFalse(pr.getOptBoolean("app.enable", Optional.of(false)).get());
		assertFalse(pr.getOptBoolean("app.enable", Optional.empty()).isPresent());
	}


	public void testGetOptionalBooleanEmpty()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.enable", " ");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertTrue(pr.getOptBoolean("app.enable", Optional.of(true)).get());
		assertFalse(pr.getOptBoolean("app.enable", Optional.of(false)).get());
		assertFalse(pr.getOptBoolean("app.enable", Optional.empty()).isPresent());
	}


	public void testGetOptionalBooleanSet_true()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.enable", "true");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertTrue(pr.getOptBoolean("app.enable", Optional.of(true)).get());
		assertTrue(pr.getOptBoolean("app.enable", Optional.of(false)).get());
		assertTrue(pr.getOptBoolean("app.enable", Optional.empty()).isPresent());
		assertTrue(pr.getOptBoolean("app.enable", Optional.empty()).get());
	}


	public void testGetOptionalBooleanSet_false()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.enable", "false");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertFalse(pr.getOptBoolean("app.enable", Optional.of(true)).get());
		assertFalse(pr.getOptBoolean("app.enable", Optional.of(false)).get());
		assertTrue(pr.getOptBoolean("app.enable", Optional.empty()).isPresent());
		assertFalse(pr.getOptBoolean("app.enable", Optional.empty()).get());
	}


	public void testGetInt()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.threadCount", "10");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(10, pr.getInt("app.threadCount"));

		try {
			pr.getInt("app.someCount");
			fail();
		} catch (PropertyParseException e) {
			assertEquals("app.someCount", e.getPropertyKey());
		}
	}


	public void testGetOptInt()
		throws Exception {

		PropertyRetriever pr = new PropertyRetriever(new Properties());
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(10, pr.getOptInt("app.threadCount", 10));
	}


	public void testGetOptIntEmpty()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.threadCount", " ");

		PropertyRetriever pr = new PropertyRetriever(new Properties());
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(10, pr.getOptInt("app.threadCount", 10));
	}


	public void testGetLong()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.timeout", "1000");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(1000L, pr.getLong("app.timeout"));

		try {
			pr.getInt("app.someTimeout");
			fail();
		} catch (PropertyParseException e) {
			assertEquals("app.someTimeout", e.getPropertyKey());
		}
	}


	public void testGetOptLong()
		throws Exception {

		PropertyRetriever pr = new PropertyRetriever(new Properties());
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(1000L, pr.getOptLong("app.timeout", 1000L));
	}


	public void testGetOptLongEmpty()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.timeout", " ");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(1000L, pr.getOptLong("app.timeout", 1000L));
	}


	public void testGetFloat()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.pi", "3.14");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(new Float("3.14"), pr.getFloat("app.pi"));

		try {
			pr.getInt("app.someFloat");
			fail();
		} catch (PropertyParseException e) {
			assertEquals("app.someFloat", e.getPropertyKey());
		}
	}


	public void testGetOptFloat()
		throws Exception {

		PropertyRetriever pr = new PropertyRetriever(new Properties());
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(new Float("3.14"), pr.getOptFloat("app.pi", 3.14f));
	}


	public void testGetString()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.name", "CoolApp");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals("CoolApp", pr.getString("app.name"));

		try {
			pr.getInt("app.someString");
			fail();
		} catch (PropertyParseException e) {
			assertEquals("app.someString", e.getPropertyKey());
		}
	}


	public void testGetOptString()
		throws Exception {

		PropertyRetriever pr = new PropertyRetriever(new Properties());
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals("CoolApp", pr.getOptString("app.name", "CoolApp"));
	}


	public void testGetOptStringEmpty()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.name", " ");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals("CoolApp", pr.getOptString("app.name", "CoolApp"));
	}


	public void testGetEnumString()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.color", "red");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		String[] colors = {"red", "green", "blue"};

		assertEquals("red", pr.getEnumString("app.color", colors));
	}


	public void testGetOptEnumString()
		throws Exception {

		PropertyRetriever pr = new PropertyRetriever(new Properties());
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		String[] colors = {"red", "green", "blue"};

		assertEquals("red", pr.getOptEnumString("app.color", colors, "red"));
	}


	public void testGetOptEnumStringEmpty()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.color", " ");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		String[] colors = {"red", "green", "blue"};

		assertEquals("red", pr.getOptEnumString("app.color", colors, "red"));
	}


	public void testGetEnum()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.timeUnit", "SECONDS");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(java.util.concurrent.TimeUnit.SECONDS, pr.getEnum("app.timeUnit", java.util.concurrent.TimeUnit.class));
	}


	public void testGetOptEnum()
		throws Exception {

		PropertyRetriever pr = new PropertyRetriever(new Properties());
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(java.util.concurrent.TimeUnit.SECONDS, 
			     pr.getOptEnum("app.timeUnit", 
			     	           java.util.concurrent.TimeUnit.class, 
			     	           java.util.concurrent.TimeUnit.SECONDS));
	}


	public void testGetOptEnumEmpty()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.timeUnit", " ");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(java.util.concurrent.TimeUnit.SECONDS, 
			     pr.getOptEnum("app.timeUnit", 
			     	           java.util.concurrent.TimeUnit.class, 
			     	           java.util.concurrent.TimeUnit.SECONDS));
	}


	public void testGetStringList()
		throws Exception {

		Properties props = new Properties();
		
		for (String value: Arrays.asList("a,b,c,d", "a b c d", "a, b, c, d")) {
			
			props.setProperty("app.list", value);
			
			PropertyRetriever pr = new PropertyRetriever(props);
			assertFalse(pr.systemPropertyOverrideIsEnabled());
			
			assertEquals(Arrays.asList("a", "b", "c", "d"), pr.getStringList("app.list"));
		}
	}


	public void testGetStringListNone() {

		Properties props = new Properties();
		
		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());
		
		try {
			pr.getStringList("app.list");
			fail();
		} catch (PropertyParseException e) {
			assertEquals("Missing property", e.getMessage());
			assertEquals("app.list", e.getPropertyKey());
			assertNull(e.getPropertyValue());
		}
	}


	public void testGetStringListEmpty() {

		Properties props = new Properties();
		props.setProperty("app.list", "");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());
		
		try {
			pr.getStringList("app.list");
			fail();
		} catch (PropertyParseException e) {
			assertEquals("Empty string list", e.getMessage());
			assertEquals("app.list", e.getPropertyKey());
			assertEquals("", e.getPropertyValue());
		}
	}


	public void testGetOptStringList()
		throws Exception {

		Properties props = new Properties();
		
		for (String value: Arrays.asList("a,b,c,d", "a b c d", "a, b, c, d")) {
			
			props.setProperty("app.list", value);
			
			PropertyRetriever pr = new PropertyRetriever(props);
			assertFalse(pr.systemPropertyOverrideIsEnabled());
			
			assertEquals(Arrays.asList("a", "b", "c", "d"), pr.getOptStringList("app.list", null));
		}
	}


	public void testGetOptStringListNone()
		throws PropertyParseException {

		Properties props = new Properties();
		
		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());
		
		List<String> def = Arrays.asList("a", "b", "c");
		assertEquals(def, pr.getOptStringList("app.list", def));
	}


	public void testGetOptStringListEmpty()
		throws PropertyParseException {

		Properties props = new Properties();
		props.setProperty("app.list", "");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());
		
		List<String> def = Arrays.asList("a", "b", "c");
		assertEquals(def, pr.getOptStringList("app.list", def));
	}


	public void testGetEnumStringList()
		throws Exception {
		
		String[] enums = new String[]{"a", "b", "c", "d", "f", "g"};

		Properties props = new Properties();
		
		for (String value: Arrays.asList("a,b,c,d", "a b c d", "a, b, c, d")) {
			
			props.setProperty("app.list", value);
			
			PropertyRetriever pr = new PropertyRetriever(props);
			assertFalse(pr.systemPropertyOverrideIsEnabled());
			
			assertEquals(Arrays.asList("a", "b", "c", "d"), pr.getEnumStringList("app.list", enums));
			
			assertEquals(Arrays.asList("a", "b", "c", "d"), pr.getEnumStringList("app.list", Arrays.asList(enums)));
		}
	}


	public void testGetEnumStringListInvalidEnum() {
		
		String[] enums = new String[]{"a", "b", "c", "d", "f", "g"};

		Properties props = new Properties();
		props.setProperty("app.list", "xxx");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());
		
		try {
			pr.getEnumStringList("app.list", enums);
			fail();
		} catch (PropertyParseException e) {
			assertEquals("Invalid enum string list property: xxx", e.getMessage());
			assertEquals("app.list", e.getPropertyKey());
			assertEquals("xxx", e.getPropertyValue());
		}
		
		try {
			pr.getEnumStringList("app.list", Arrays.asList(enums));
			fail();
		} catch (PropertyParseException e) {
			assertEquals("Invalid enum string list property: xxx", e.getMessage());
			assertEquals("app.list", e.getPropertyKey());
			assertEquals("xxx", e.getPropertyValue());
		}
	}


	public void testGetEnumStringListNone() {
		
		String[] enums = new String[]{"a", "b", "c", "d", "f", "g"};

		Properties props = new Properties();
		
		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());
		
		try {
			pr.getEnumStringList("app.list", enums);
			fail();
		} catch (PropertyParseException e) {
			assertEquals("Missing property", e.getMessage());
			assertEquals("app.list", e.getPropertyKey());
			assertNull(e.getPropertyValue());
		}
		
		try {
			pr.getEnumStringList("app.list", Arrays.asList(enums));
			fail();
		} catch (PropertyParseException e) {
			assertEquals("Missing property", e.getMessage());
			assertEquals("app.list", e.getPropertyKey());
			assertNull(e.getPropertyValue());
		}
	}


	public void testGetEnumStringListEmpty() {
		
		String[] enums = new String[]{"a", "b", "c", "d", "f", "g"};

		Properties props = new Properties();
		props.setProperty("app.list", "");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());
		
		try {
			pr.getEnumStringList("app.list", enums);
			fail();
		} catch (PropertyParseException e) {
			assertEquals("Empty string list", e.getMessage());
			assertEquals("app.list", e.getPropertyKey());
			assertEquals("", e.getPropertyValue());
		}
		
		try {
			pr.getEnumStringList("app.list", Arrays.asList(enums));
			fail();
		} catch (PropertyParseException e) {
			assertEquals("Empty string list", e.getMessage());
			assertEquals("app.list", e.getPropertyKey());
			assertEquals("", e.getPropertyValue());
		}
	}


	public void testGetOptEnumStringList()
		throws Exception {
		
		String[] enums = new String[]{"a", "b", "c", "d", "f", "g"};

		Properties props = new Properties();
		
		for (String value: Arrays.asList("a,b,c,d", "a b c d", "a, b, c, d")) {
			
			props.setProperty("app.list", value);
			
			PropertyRetriever pr = new PropertyRetriever(props);
			assertFalse(pr.systemPropertyOverrideIsEnabled());
			
			assertEquals(Arrays.asList("a", "b", "c", "d"), pr.getOptEnumStringList("app.list", enums, null));
			
			assertEquals(Arrays.asList("a", "b", "c", "d"), pr.getOptEnumStringList("app.list", Arrays.asList(enums), null));
		}
	}
	
	
	public void testGetOptEnumStringListInvalidEnum() {
		
		String[] enums = new String[]{"a", "b", "c", "d", "f", "g"};
		
		Properties props = new Properties();
		props.setProperty("app.list", "xxx");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());
		
		try {
			pr.getOptEnumStringList("app.list", enums, null);
			fail();
		} catch (PropertyParseException e) {
			assertEquals("Invalid enum string list property: xxx", e.getMessage());
			assertEquals("app.list", e.getPropertyKey());
			assertEquals("xxx", e.getPropertyValue());
		}
		
		try {
			pr.getOptEnumStringList("app.list", Arrays.asList(enums), null);
			fail();
		} catch (PropertyParseException e) {
			assertEquals("Invalid enum string list property: xxx", e.getMessage());
			assertEquals("app.list", e.getPropertyKey());
			assertEquals("xxx", e.getPropertyValue());
		}
	}


	public void testGetOptEnumStringListNone()
		throws PropertyParseException {
		
		String[] enums = new String[]{"a", "b", "c", "d", "f", "g"};

		Properties props = new Properties();
		
		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());
		
		List<String> def = Arrays.asList("a", "b", "c");
		
		assertEquals(def, pr.getOptEnumStringList("app.list", enums, def));
		
		assertEquals(def, pr.getOptEnumStringList("app.list", Arrays.asList(enums), def));
	}


	public void testGetOptEnumStringListEmpty()
		throws PropertyParseException {
		
		String[] enums = new String[]{"a", "b", "c", "d", "f", "g"};

		Properties props = new Properties();
		props.setProperty("app.list", "");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());
		
		List<String> def = Arrays.asList("a", "b", "c");
		
		assertEquals(def, pr.getOptEnumStringList("app.list", enums, def));
		
		assertEquals(def, pr.getOptEnumStringList("app.list", Arrays.asList(enums), def));
	}


	public void testGetOptEnumStringListEmpty_defaultEmpty()
		throws PropertyParseException {
		
		String[] enums = new String[]{"a", "b", "c", "d", "f", "g"};

		Properties props = new Properties();
		props.setProperty("app.list", "");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());
		
		List<String> def = Collections.emptyList();
		
		assertEquals(def, pr.getOptEnumStringList("app.list", enums, def));
		
		assertEquals(def, pr.getOptEnumStringList("app.list", Arrays.asList(enums), def));
	}
	
	
	public void testGetStringListMulti_one()
		throws PropertyParseException {
		
		Properties props = new Properties();
		props.setProperty("app.list.1", "a");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		
		List<String> values = pr.getStringListMulti("app.list.");
		assertEquals(Collections.singletonList("a"), values);
	}
	
	
	public void testGetStringListMulti_two()
		throws PropertyParseException {
		
		Properties props = new Properties();
		props.setProperty("app.list.1", "a");
		props.setProperty("app.list.2", "b");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		
		List<String> values = pr.getStringListMulti("app.list.");
		assertTrue(values.contains("a"));
		assertTrue(values.contains("b"));
		assertEquals(2, values.size());
	}
	
	
	public void testGetStringListMulti_three()
		throws PropertyParseException {
		
		Properties props = new Properties();
		props.setProperty("app.list.1", "a");
		props.setProperty("app.list.2", "b");
		props.setProperty("app.list.3", "c");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		
		List<String> values = pr.getStringListMulti("app.list.");
		assertTrue(values.contains("a"));
		assertTrue(values.contains("b"));
		assertTrue(values.contains("c"));
		assertEquals(3, values.size());
	}
	
	
	public void testGetStringListMulti_empty() {
		
		Properties props = new Properties();
		props.setProperty("app.list.1", "");
		props.setProperty("app.list.2", "");
		props.setProperty("app.list.3", "");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		
		try {
			pr.getStringListMulti("app.list.");
			fail();
		} catch (PropertyParseException e) {
			assertEquals("Missing properties", e.getMessage());
			assertEquals("app.list.*", e.getPropertyKey());
			assertNull(e.getPropertyValue());
		}
	}
	
	
	public void testGetStringListMulti_blank() {
		
		Properties props = new Properties();
		props.setProperty("app.list.1", " ");
		props.setProperty("app.list.2", " ");
		props.setProperty("app.list.3", " ");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		
		try {
			pr.getStringListMulti("app.list.");
			fail();
		} catch (PropertyParseException e) {
			assertEquals("Missing properties", e.getMessage());
			assertEquals("app.list.*", e.getPropertyKey());
			assertNull(e.getPropertyValue());
		}
	}
	
	
	public void testOptGetStringListMulti_one() {
		
		Properties props = new Properties();
		props.setProperty("app.list.1", "a");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		
		List<String> values = pr.getOptStringListMulti("app.list.", null);
		assertEquals(Collections.singletonList("a"), values);
	}
	
	
	public void testOptGetStringListMulti_two() {
		
		Properties props = new Properties();
		props.setProperty("app.list.1", "a");
		props.setProperty("app.list.2", "b");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		
		List<String> values = pr.getOptStringListMulti("app.list.", null);
		assertTrue(values.contains("a"));
		assertTrue(values.contains("b"));
		assertEquals(2, values.size());
	}
	
	
	public void testOptGetStringListMulti_none() {
		
		Properties props = new Properties();
		
		PropertyRetriever pr = new PropertyRetriever(props);
		
		assertNull(pr.getOptStringListMulti("app.list.", null));
	}
	
	
	public void testOptGetStringListMulti_empty() {
		
		Properties props = new Properties();
		props.setProperty("app.list.1", "");
		props.setProperty("app.list.2", "");
		props.setProperty("app.list.3", "");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		
		assertNull(pr.getOptStringListMulti("app.list.", null));
	}
	
	
	public void testGetOptStringListMulti_blank() {
		
		Properties props = new Properties();
		props.setProperty("app.list.1", " ");
		props.setProperty("app.list.2", " ");
		props.setProperty("app.list.3", " ");
		
		PropertyRetriever pr = new PropertyRetriever(props);
		
		assertNull(pr.getOptStringListMulti("app.list.", null));
	}


	public void testGetURL()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.url", "http://app.com");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(new URL("http://app.com"), pr.getURL("app.url"));
	}


	public void testGetOptURL()
		throws Exception {

		PropertyRetriever pr = new PropertyRetriever(new Properties());
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(new URL("http://app.com"), pr.getOptURL("app.url", new URL("http://app.com")));
	}


	public void testGetOptURLEmpty()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.url", " ");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(new URL("http://app.com"), pr.getOptURL("app.url", new URL("http://app.com")));
	}


	public void testGetURI()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.uri", "http://app.com");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(new URI("http://app.com"), pr.getURI("app.uri"));
	}


	public void testGetOptURI()
		throws Exception {

		PropertyRetriever pr = new PropertyRetriever(new Properties());
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(new URI("http://app.com"), pr.getOptURI("app.url", new URI("http://app.com")));
	}


	public void testGetOptURIEmpty()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("app.url", " ");

		PropertyRetriever pr = new PropertyRetriever(props);
		assertFalse(pr.systemPropertyOverrideIsEnabled());

		assertEquals(new URI("http://app.com"), pr.getOptURI("app.url", new URI("http://app.com")));
	}


	public void testSystemPropertyOverride()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("enableReporting", "true");
		System.setProperty("enableReporting", "false");

		assertEquals("false", System.getProperty("enableReporting"));

		PropertyRetriever pr = new PropertyRetriever(props, true);
		assertTrue(pr.systemPropertyOverrideIsEnabled());

		assertFalse(pr.getBoolean("enableReporting"));
	}
}