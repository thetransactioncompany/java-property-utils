package com.thetransactioncompany.util;


import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


/**
 * Property filtering utilities.
 *
 * @author Vladimir Dzhuvinov
 */
public class PropertyFilter {
	


	/**
	 * Filters properties with the specified name prefix.
	 *
	 * @param prefix The property name prefix to filter matching
	 *               properties. Must not be {@code null}.
	 * @param props  The properties to filter, {@code null}
	 *
	 * @return The filtered properties, empty if no matches were found or
	 *         the original properties are empty or {@code null}.
	 */
	public static Properties filterWithPrefix(final String prefix, final Properties props) {

		Properties filteredProps = new Properties();

		if (props == null || props.isEmpty()) {
			return filteredProps;
		}

		for (String key: props.stringPropertyNames()) {

			if (key.startsWith(prefix)) {
				filteredProps.put(key, props.getProperty(key));
			}
		}

		return filteredProps;
	}
	
	
	/**
	 * Filters properties with the specified name prefix into a map where
	 * the map key is the first dot-delimited segment of the remaining
	 * property name substring.
	 *
	 * <p>Example properties input, with common prefix "config.client.":
	 *
	 * <pre>
	 * config.client.1.url=https://example.com/token
	 * config.client.1.clientID=123
	 * config.client.1.auth=client_secret_basic
	 * config.client.1.secret=quahPho9juto
	 * config.client.2.url=https://example.org/token
	 * config.client.2.clientID=456
	 * config.client.2.auth=client_secret_basic
	 * config.client.2.secret=AhshiSabae2k
	 * </pre>
	 *
	 * <p>Output:
	 *
	 * <pre>
	 * 1 => {
	 * 	url=https://example.com/token
	 * 	clientID=123
	 * 	auth=client_secret_basic
	 * },
	 * 2 => {
	 *      url=https://example.org/token
	 *      clientID=456
	 *      auth=client_secret_basic
	 *      secret=AhshiSabae2k
	 * }
	 * </pre>
	 *
	 * @param prefix The common property name prefix to filter
	 *               matching properties. Must not be {@code null}.
	 * @param props  The properties to filter, {@code null}
	 *
	 * @return The map of filtered properties, empty if no matches were
	 *         found or the original properties are empty or {@code null}.
	 */
	public static Map<String, Properties> filterWithPrefixIntoMap(final String prefix, final Properties props) {
		
		Properties filteredProps = filterWithPrefix(prefix, props);
		
		Map<String, Properties> map = new HashMap<>();
		
		for (String propName: filteredProps.stringPropertyNames()) {
			
			// Extract map key
			String suffix = propName.substring(prefix.length());
			
			if (! suffix.contains(".")) {
				continue; // skip
			}
			
			String mapKey = suffix.substring(0, suffix.indexOf("."));
			String keyedPropName = suffix.substring(suffix.indexOf(".") + 1);
			
			if (keyedPropName.isEmpty()) {
				continue; // skip
			}
			
			// Get / create properties as map value
			Properties keyedProps = map.get(mapKey);
			if (keyedProps == null) {
				keyedProps = new Properties();
			}
			
			// Update
			keyedProps.setProperty(keyedPropName, props.getProperty(propName));
			
			map.put(mapKey, keyedProps);
		}
		
		return map;
	}
}
